package com.company.Slice1;

import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Created by root on 12.06.17.
 */
public class TaskQueue {

    private static ConcurrentLinkedQueue<Integer>
            queue = new ConcurrentLinkedQueue<>();

    public static void pushToQueue(Integer val){
        queue.add(val);
    }

    public static Integer getTask(){
        return queue.poll();
    }




}
