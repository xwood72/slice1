package com.company.Slice1;

public class Main {

    public static volatile int n = 0;
    public static Object anchor  = new Object();
    public static volatile int nUniques = 0;

    public static void main(String[] args) {
	// write your code here

        NumberStorage storage = new NumberStorage();
        NumberGenerator generator = new NumberGenerator();
        Worker worker = new Worker();

        generator.start();
        worker.start();

    }
}
