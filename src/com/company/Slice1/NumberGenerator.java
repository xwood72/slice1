package com.company.Slice1;

import java.util.Random;

/**
 * Created by root on 12.06.17.
 */
public class NumberGenerator extends Thread {

    @Override
    public void run() {

        Random r = new Random();
        int count = 0;
        while (Main.n < 99) {
            int val = r.nextInt(100);
            System.out.println("Generate new val: " + val);
            NumberStorage.save(val);
            count++;

            if (count % 5 == 0) {
                synchronized (Main.anchor) {
                    Main.anchor.notifyAll();
                }
            }
            try {
                sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            Main.n++;
        }


    }
}

