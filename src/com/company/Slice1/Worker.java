package com.company.Slice1;

/**
 * Created by root on 12.06.17.
 */
public class Worker extends Thread {

    @Override
    public void run() {

        while (Main.n < 99) {
            synchronized (Main.anchor) {
                try {
                    Main.anchor.wait();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
            System.out.println("Unics count: " + (Main.n+1 - NumberStorage.getUnicsNumber()));
        }
    }
}


