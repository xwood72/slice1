package com.company.Slice1;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Created by root on 12.06.17.
 */
public class NumberStorage {

    private static java.util.concurrent.ConcurrentHashMap<Integer, Integer> map
    = new ConcurrentHashMap<Integer, Integer>();

    public static void save(int val){
        map.put(val, val);
    }

    public static int getUnicsNumber(){
        return map.size();
    }
}
